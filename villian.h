/*
 * Makes the villian  object class specs
 */

#ifndef _VILLIAN_H
#define _VILLIAN_H

#include "object.h"

class Villian: public Object
{
public:
    void action(Object*);
        // Take action on another object

    void takeDamage(int damage);
        // Decrease instance health

    void setMove(const char*, int);
        //sets the move
    

    Villian(const char* buff="", int init_health=0);
    // destructor, deallocates moves and name
    ~Villian();
private:
    MOVE* attack;


};

#endif


