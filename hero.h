/* Spec for hero 
 * inherits from object
 */

#ifndef _HERO_H
#define _HERO_H

#include "object.h"

class Hero: public Object
{
public:
    void action(Object *);
    void takeDamage(int damage);
    void setMove(const char *buff=0, int init_health=0);
    void printMoves();
        // gives dialog of number and types of moves

    // constructor
    Hero(const char * str, int);

    ~Hero();
        // deallocates moves

private:
    MOVE **attacks;
        // holds moves, must deallocate in destructor
    int numberOfMoves;
        // Counts number of moves


};

#endif
