// Implements not virtual functions from object.h
// Inherited classes must implement other functions described
// in object.h, including the destructor!
#include <cstring>
#include <iostream>
using namespace std;

#include "object.h"

bool Object::isFaint()
{
    if ( !(health > 0))
        return true;
    return false;
}

Object::Object(const char * buff, int init_health)
{
    int buffLength = strlen(buff);
    name = new char[buffLength + 1];
    if (!name) {
        cout << "Memory allocation failure!" << endl;
    }
    strcpy(name, buff);
    health = init_health;
}

Object::~Object()
{
    if (name) {
        delete [] name;
    }
}

int Object::getHealth()
{
    
    return health;
}

void Object::setHealth(int new_health)
{
    //don't allow negative healths
    if (new_health < 0)
        health = 0;
    else
        health = new_health;
}

bool Object::isCritical()
{
    return health < .10 * health_start;
}

// Overload the ostream and print the objects name
ostream& operator<< (ostream& osObject,
                     const Object& object)
{
    osObject << object.name;
    return osObject;
}

