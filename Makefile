CFLAGS= -g -Wall
CC = g++

main: main.o hero.o villian.o object.o

main.o: main.cpp hero.cpp villian.cpp object.cpp

villian.o: villian.cpp villian.h object.h

hero.o: hero.cpp hero.h object.h

object.o: object.h object.cpp

clean:
	rm -f *.o
