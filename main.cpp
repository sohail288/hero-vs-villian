/* Simple program to test classes, inheritance, op-overloading
 * and dynamic memory allocation
 *
 * Main program features a simple battle between
 * hero and villian
 *
 * Hero can attack in various ways
 * Villian can attack in one way
 *
 * while loop ends when either villian or hero faints
 *
 * Sohail Khan
 *
 */

#include <iostream>
#include <iomanip>
#include <cstdlib>
    // includes rand
using namespace std;
#include "hero.h"
#include "villian.h"

void displayStats(Hero * player, Villian * boss);

int main()
{ 
    char rv;
    // Set up hero and villian
    //cout << "Do this" << endl;
    //cin >> rv;
    Hero *player;
    Villian *boss;
    player = new Hero("Wizard", 60);
    if (!player) {
        cout << "Unable to allocate memory" << endl;
    }
    boss = new Villian("Dragon", 100); 
    if (!player) {
        cout << "unable to allocate memory" << endl;
    }
    /*cout << "Hero and Villian created" << endl;
    cin >> rv;*/
    // set moves
    boss->setMove("Fireball", 13);
    /*cout << "First move created" << endl;
    cin >> rv;*/
    player->setMove("Ice", 15);
    player->setMove("Fire", -15);
    player->setMove("Water", 20);

/*    cout << "2 Moves Created" << endl;*/

    cout << *player << "vs" << *boss << endl;
    cout << "press enter to start";
    cin >> rv;
    while(1) {
       
        if (player->isFaint() || boss->isFaint())
            break;
        displayStats(player, boss);
        player->action(boss);
        boss->action(player);

    }

    if (player->isFaint()) {
        cout << *player << " loses" << endl;
    } else
        cout << *boss << " loses." << endl;

    return 0;
}


void displayStats(Hero *player, Villian *boss)
{

    cout << *player << endl << " Health: " 
         << player->getHealth() << endl;
    cout << *boss << endl << " Health: " 
         << boss->getHealth() << endl;

}
        


        
