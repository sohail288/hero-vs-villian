/* Parent class of villian and hero
 *
 * Pure virtual class
 *
 * Inheritors need to implement these functions
 */

#ifndef _OBJECT_H
#define _OBJECT_H

typedef struct {
    char *name;
    int power;
} MOVE;

class Object
{
friend ostream& operator<<(ostream&, const Object&);

public:
    // pure virtual functions
    virtual void action(Object*) = 0;
        // Function to take action on another derived
        // object.
    virtual void takeDamage(int damage) = 0;
       // Function to decrease health
    virtual void setMove(const char*, int) = 0;
        // setMove
    bool isFaint();
        // Function to check to see if object is faint or not
    bool isCritical();
    int getHealth();
        // gets the health for instance
    void setHealth(int);
        // sets the health for instance
        // postcondition: health is set

    // constructor
    Object(const char * buff= "", int init_health = 0);
    

    // destructor deallocates name and other allocated memry
    virtual ~Object();

private:
    int health;
    int health_start;
    char * name;
};


// REMEMBER TO DESTROY MOVES in DESTRUCTOR
#endif
