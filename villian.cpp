/* implements definitions for villian class in villian.h
 * */

#include <iostream>
#include <cstdlib>
#include <cstring>
using namespace std;

#include "villian.h"

Villian::Villian(const char * buff, int init_health)
        :Object(buff, init_health)
{
    //Base constructor called
}

void Villian::setMove(const char *buff, int power)
{
    MOVE *newMove = new MOVE;
    if (!newMove) {
        cout << "Memory allocation failure" << endl;
        std::exit(1);
    }
    newMove->name = new char[strlen(buff)+1];
    newMove->power = power;
    if (!newMove->name) {
        cout << "Memory allocation failure" << endl;
        std::exit(1);
    }
    strcpy(newMove->name, buff);
    attack = newMove;
}

Villian::~Villian()
{
    //delete the move
    if (attack) {
        delete [] attack->name;
        delete attack;
    }
}

    
void Villian::action(Object* hero)
{
    if (isFaint()) {
        return;
    }
    int damage = attack->power;
    float randomMultiplier = 1.2;

    cout << *this << " attacks " << *hero << " with "
         << attack->name   << endl;
    
    // figure out if damage is to be multiplied
    if (isCritical()) {
        // More of chance to get critical damage
        if (rand() % 3) {
            damage = damage*randomMultiplier +.5;
        }
     
    // less of a chance to get critical damage
    } else {
        if (!(rand() % 10))
            damage = damage*randomMultiplier + .5;
    }

    // allow missing
    if (rand() % 5) {
        cout << *hero << " takes " << damage 
             << " points damage"<< endl;
        hero->takeDamage(damage);
    } else {
        cout << *this << " missed!" << endl;
    }
}
    
void Villian::takeDamage(int damage)
{
    int currentHealth = getHealth() - damage;
    setHealth(currentHealth);
}

// provide getHealth, setHealth, operator overloading in base
// Overload the stream operator!
