/*
 * hero.cpp
 *
 * implements declarations found in hero.h
 */
#include <iomanip>
#include <cstdlib>
#include <cstring>
#include <iostream>
using namespace std;

#include "hero.h"

Hero::Hero(const char * buff, int init_health)
      : Object(buff, init_health)
{
    // Default Constructor calls base class
    // Then it sets numberOfMoves to zero
    numberOfMoves = 0;
}

void Hero::setMove(const char *buff, int power)
    //Adds an attack to the existing list of attacks
    //Deletes old attacks and then assigns attack pointer
    //to new array
{
    int rv;
    cout << "Current moves: " << numberOfMoves << endl;
        //This should give number of pointers in attacks

    // steps to add a new attack.
    // 1. Make a new array that holds all previous attacks +1
    // 2. Copy over all attacks to new array
    // 2. Make a new attack
    // 3. assign new attack to last position
    // 4. delete old array
    //
    MOVE **newAttacks = new MOVE* [numberOfMoves+1];
    if (!newAttacks) {
        cout << "memory allocation failure" << endl;
    }
    // this moves the pointers to the previous attacks
    // to a new location 
    cin >>rv; 
    cout << sizeof(attacks[0]) << endl;
    cin >> rv;
    
    if (attacks) {
        cout << "Moving memory" << endl;
        cout << numberOfMoves << endl;
        cin >> rv;
        memmove(newAttacks, attacks, 
            sizeof(attacks[0]) *numberOfMoves);
    }
    
    MOVE *newAttack = new MOVE;
    if (!newAttack) {
        cout <<"memory allocation failure" << endl;
    }
    newAttack->name = new char[strlen(buff)+1];
    strcpy(newAttack->name, buff);
    newAttack->power = power;

    newAttacks[numberOfMoves] = newAttack;

    if (attacks) {
        delete [] attacks;
    }
    attacks = newAttacks;
    // attack now points to newly created attacks
    // hopefully doing delete on attacks didn't affect this
    numberOfMoves++; // increment number of attacks
    cout << "Number of attacks: " << numberOfMoves << endl;
    
}

// destructor deallocates moves
Hero::~Hero()
{
    // deallocate all names for attacks.
    // deallocate attacks
    // deallocate array
    for (int i = 0; i <numberOfMoves; i++) {
        if (attacks[i]) {
            delete [] attacks[i]->name;
            delete attacks[i];
        }
    }
    delete [] attacks;
}

void Hero::action(Object* villian)
{
    if (isFaint()) {
        // player can't move
        return;
    }
    int damage = 0;
    int moveSelect; // Given a list of moves, stores move
    MOVE *attack; //pointer to the mv to be used

    cout << "Moves for " << *this << endl;
    for(int i = 0; i < numberOfMoves; i++) {
        cout << setw(2) << i+1 << attacks[i]->name << endl;
    }
    cout << "Select your move: " << endl;
    cin >> moveSelect;
    
    if (moveSelect >= 1 && moveSelect <= numberOfMoves) {
        attack = attacks[moveSelect - 1];
        cout << *this << " attacks " << *villian << " with "
             << attack->name;
        damage = attack-> power;

        // allow missing
        if (!(rand()%10)) {
            cout << "Attack missed!" << endl;
        } else {
            villian->takeDamage(damage);
            cout << *villian << " takes " << damage << " of"
                 << " damage" << endl;
        }
    } else {
        cout << "No move exists, you just skipped your turn" 
             << endl;
    }


}

// make takeDamage
void Hero::takeDamage(int damage)
{
    int currentHealth = this->getHealth() - damage;
    this->setHealth(currentHealth);

}
